;Arg1 RCX
;Arg2 RDX
;Arg3 R8
;Arg4 R9

;The x64 ABI considers the registers RAX, RCX, RDX, R8, R9, R10, R11,
;and XMM0-XMM5 volatile.
;When present, the upper portions of YMM0-YMM15 and ZMM0-ZMM15 are also volatile.
;On AVX512VL, the ZMM, YMM, and XMM registers 16-31 are also volatile.
;Consider volatile registers destroyed on function calls unless otherwise 
;safety-provable by analysis such as whole program optimization.
;The x64 ABI considers registers RBX, RBP, RDI, RSI, RSP, R12, R13, R14, R15,
;and XMM6-XMM15 nonvolatile.
;They must be saved and restored by a function that uses them.

EXTERN malloc:PROC

.data

B64CODES		db "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
B64CODES_LEN	equ $ - B64CODES

.code

b64IsValid proc
; do not export, this procedure is not abi compliant - argument is in rax register
; params:
;	al - value to be checked
; return:
;	al - unchanged value if valid, ffh if value is invalid
;	ah - offset of B64 code if valid, ffh if value is invalid
	mov ah, 63
	cmp al, '/'
	je b64IsValidValidVal
	mov ah, 62
	cmp al, '+'
	je b64IsValidValidVal
	xor ah, ah
	cmp al, '='
	je b64IsValidValidVal
	; skip first 26 B64CODES
	mov ah, al
	add ah, 26
	sub ah, 'a'
	cmp al, 'z'
	ja b64IsValidInvalidVal
	cmp al, 'a'
	jnb b64IsValidValidVal
	mov ah, al
	sub ah, 'A'
	cmp al, 'Z'
	ja b64IsValidInvalidVal
	cmp al, 'A'
	jnb b64IsValidValidVal
	mov ah, al
	add ah, 52
	sub ah, '0'
	cmp al, '9'
	ja b64IsValidInvalidVal
	cmp al, '0'
	jb b64IsValidInvalidVal

b64IsValidValidVal:
	mov al, ah
	ret

b64IsValidInvalidVal:
	mov ax, 0ffffh
	ret
b64IsValid endp

base64Decode proc
; Procedure decodes base64 encoded data
; params:
;	rcx -const char *b64Buf, 
;	rdx - size_t b64Len,
;	r8 - char * decBuf, 
;	r9 - size_t decLen
; return:
;	rax - decBuf length as int
	push rbx

	mov rax, rdx
	and rax, 3
	jnz base64DecodeInvalidSize

	xor rbx, rbx
	xor r10, r10
base64DecodeChunk:
	mov al, byte ptr [rcx]
	call b64IsValid
	cmp ax, 0ffffh
	je base64DecodeInvalidValue

	shl r10, 6
	add r10b, al
	inc rbx
	inc rcx
	dec rdx

	cmp rbx, 4
	jb base64DecodeChunk

	mov byte ptr [r8+2], r10b
	shr r10, 8
	mov byte ptr [r8+1], r10b
	shr r10, 8
	mov byte ptr [r8], r10b
	add r8, 3
	add r11, 3

	xor rbx, rbx
	xor r10, r10
	cmp rdx, 0
	ja base64DecodeChunk

	mov rax, r11
	pop rbx
	ret

base64DecodeInvalidSize:
	mov rax, -1
	pop rbx
	ret

base64DecodeInvalidValue:
	mov rax, -2
	pop rbx
	ret

base64Decode endp

hexIsValid proc
; do not export, this procedure is not abi compliant - argument is in rax register
; params:
;	al - value to be checked
; return:
;	al - uchanged is value if valid, ffh if value is invalid
;	ah - offset of hex value if al is valid, ffh if value is invalid
	mov ah, 57h
	cmp al, 'f'
	ja hexIsValidInvalidHexVal
	cmp al, 'a'
	jnb hexIsValidValidHexVal
	mov ah, 37h
	cmp al, 'F'
	ja hexIsValidInvalidHexVal
	cmp al, 'A'
	jnb hexIsValidValidHexVal
	mov ah, 30h
	cmp al, '9'
	ja hexIsValidInvalidHexVal
	cmp al, '0'
	jb hexIsValidInvalidHexVal

hexIsValidValidHexVal:
	ret

hexIsValidInvalidHexVal:
	mov ax, 0ffffh
	ret
hexIsValid endp

hexToBytes proc
; Procedure converts hex string into bytes
; params:
;	rcx -const char *hexBuf, 
;	rdx - size_t hexLen
; return:
;	rax - lenght of data as int if hexstring is valid
;		  -1 if hexstring is invalid
;		  -2 if hexstring size is invalid
	mov rax, rdx
	and rax, 1
	jnz hexToBytesInvalidSize

	mov r8, rcx
	xor r10, r10

hexToBytesNextByte:
	mov al, byte ptr[rcx]
	call hexIsValid
	cmp ax, 0ffffh
	je hexToBytesInvalidData
	sub al, ah
	mov r9b, al

	mov al, byte ptr[rcx+1]
	call hexIsValid
	cmp ax, 0ffffh
	je hexToBytesInvalidData
	sub al, ah
	shl r9b, 4
	add r9b, al
	mov byte ptr[r8], r9b
	
	inc r8
	inc r10
	add rcx, 2
	sub rdx, 2
	cmp rdx, 0
	ja hexToBytesNextByte

	mov rax, r10
	ret

hexToBytesInvalidData:
	mov rax, -1
	ret
hexToBytesInvalidSize:
	mov rax, -2
	ret
hexToBytes endp

hexToBase64 proc
; Procedure converts hex string into bytes and encodes tha data to base64
; params:
;	rcx -const char *hexBuf, 
;	rdx - size_t hexLen,
;	r8 - char * b64Buf, 
;	r9 - size_t b64Len
; return:
;	rax - b64Buf length as int
	push rbx

	mov rax, rdx
	and rax, 1
	jnz hexToBase64invalidHexLen

	; how many bytes in result
	xor r11, r11
	; how many bytes in chunk
	xor rbx, rbx
	xor r10, r10
hexToBase64Loop:
	
	mov al, byte ptr[rcx]

	call hexIsValid
	cmp ax, 0ffffh
	je hexToBase64invalidHexVal

	sub al, ah
	shl r10, 4
	add r10b, al

	inc rcx
	dec rdx

	mov al, byte ptr[rcx];
	call hexIsValid
	cmp ax, 0ffffh
	je hexToBase64invalidHexVal

	sub al, ah
	shl r10, 4
	add r10b, al

	inc bl
	inc rcx
	dec rdx

	cmp rdx, 0
	je hexToBase64EncodeChunk
	cmp bl, 3
	jb hexToBase64Loop

hexToBase64EncodeChunk:

	lea rax, B64CODES

	cmp bl, 3
	je hexToBase64EncodeChunkSkip0
	shl r10, 2
	inc r11
	mov byte ptr[r8+3], '='
	cmp bl, 2
	je hexToBase64EncodeChunkSkip1
	shl r10, 2
	inc r11
	mov byte ptr[r8+2], '='
	jmp hexToBase64EncodeChunkSkip2

hexToBase64EncodeChunkSkip0:
	
	xor rbx, rbx
	mov bl, r10b
	shr r10, 6
	and bl, 03fh
	add rbx, rax
	mov bl, byte ptr[rbx]
	mov byte ptr[r8+3], bl
	inc r11

hexToBase64EncodeChunkSkip1:

	xor rbx, rbx
	mov bl, r10b
	shr r10, 6
	and bl, 03fh
	add rbx, rax
	mov bl, byte ptr[rbx]
	mov byte ptr[r8+2], bl
	inc r11

hexToBase64EncodeChunkSkip2:
	xor rbx, rbx
	mov bl, r10b
	shr r10, 6
	and bl, 03fh
	add rbx, rax
	mov bl, byte ptr[rbx]
	mov byte ptr[r8+1], bl
	inc r11

	xor rbx, rbx
	mov bl, r10b
	shr r10, 6
	and bl, 03fh
	add rbx, rax
	mov bl, byte ptr[rbx]
	mov byte ptr[r8+0], bl
	inc r11

	add r8, 4

	xor rbx, rbx
	cmp rdx, 0
	ja hexToBase64Loop

	mov rax, r11
	pop rbx
	ret

hexToBase64invalidHexLen:
	mov rax, -1
	pop rbx
	ret

hexToBase64invalidHexVal:
	mov rax, -2
	pop rbx
	ret

hexToBase64 endp

xorArrays proc
; Procedure xor two Arrays values and return new allocated array with result
; params:
;	rcx -const unsigned char *firstArray, 
;	rdx - size_t firstArrayLen,
;	r8 - const unsigned char * secondArray, 
;	r9 - size_t secondArrayLen
; return:
;	rax - pointer to array with xored values
	mov r10, rcx
	mov rcx, rdx
	cmp rdx, r9
	ja xorArraysAllocateMemory
	mov rcx, r9
xorArraysAllocateMemory:
	push r10
	push rdx
	push r8
	push r9
	sub rsp, 20h
	call malloc
	add rsp, 20h
	pop r9
	pop r8
	pop rdx
	pop r10
	mov r11, rax
xorArraysDoXor:
	xor rcx, rcx
	cmp rdx, 0
	jna xorArraysSkipFirst
	mov cl, byte ptr[r10]
	dec rdx
	inc r10
xorArraysSkipFirst:
	cmp r9, 0
	jna xorArraysSkipSecond
	xor cl, byte ptr[r8]
	dec r9
	inc r8
xorArraysSkipSecond:
	mov byte ptr[r11], cl
	inc r11

	cmp rdx, 0
	ja xorArraysDoXor
	cmp r9, 0
	ja xorArraysDoXor

	ret
xorArrays endp

end
