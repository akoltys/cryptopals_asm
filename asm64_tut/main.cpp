#include <iostream>
#include <memory>

using namespace std;

extern "C" int GetValueFromASM();
extern "C" int SubValueFromASM(int64_t x);

extern "C" int hexToBytes(char* hexBuf, size_t hexLen);
extern "C" int hexToBase64(const char* hexBuf, size_t hexLen, char *b64Buf, size_t b64Len);
extern "C" int base64Decode(const char* b64Buf, size_t b64Len, char* decBuf, size_t decLen);
extern "C" uint8_t* xorArrays(const uint8_t * buf1, const size_t buf1Len, const uint8_t* buf2, const size_t buf2Len);


void CryptopalsChallenge1()
{
	cout << "Cryptopals challenge 1" << endl;
	string hexData = "49276d206b696c6c696e6720796f757220627261696e206c696b6" \
					 "5206120706f69736f6e6f7573206d757368726f6f6d";
	string baseBuf;
	baseBuf.resize(hexData.size(), 0x0);
	auto newSize = hexToBase64(hexData.c_str(), hexData.size(), 
							   &baseBuf[0], baseBuf.size());
	if (newSize > 0) {
		baseBuf.resize(newSize);
		cout << "Expected: " << "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc"
			 << "29ub3VzIG11c2hyb29t" << endl;
		cout << "Got:      " << baseBuf << endl;
	}
	else {
		cout << "Error: " << newSize << endl;
	}
}

void CryptopalsChallenge2()
{
	cout << "Cryptopals challenge 2" << endl;
	uint8_t buf1[] = { 0x1c, 0x01, 0x11, 0x00, 0x1f, 0x01, 0x01, 0x00, 0x06, \
					   0x1a, 0x02, 0x4b, 0x53, 0x53, 0x50, 0x09, 0x18, 0x1c };
	uint8_t buf2[] = { 0x68, 0x69, 0x74, 0x20, 0x74, 0x68, 0x65, 0x20, 0x62, \
					   0x75, 0x6c, 0x6c, 0x27, 0x73, 0x20, 0x65, 0x79, 0x65 };
	uint8_t expt[] = { 0x75, 0x68, 0x65, 0x20, 0x6b, 0x69, 0x64, 0x20, 0x64, \
					   0x6f, 0x6e, 0x27, 0x74, 0x20, 0x70, 0x6c, 0x61, 0x79 };
	uint8_t* tmp = xorArrays(buf1, sizeof(buf1), buf2, sizeof(buf2));
	unique_ptr<uint8_t> tmpArr(tmp);
	cout << "Result:   [ ";
	for (auto idx = 0; idx < sizeof(buf1) && tmpArr.get() != nullptr; idx++) {
		auto tmpVal = static_cast<unsigned int>(tmpArr.get()[idx]);
		cout << hex << tmpVal << ",   ";
	}
	cout << "]" << endl;

	cout << "Expected: [ ";
	for (auto idx = 0; idx < sizeof(buf1) && tmpArr.get() != nullptr; idx++) {
		auto tmpVal = static_cast<unsigned int>(expt[idx]);
		if (expt[idx] != tmpArr.get()[idx]) {
			cout << hex << "!" << tmpVal << "!, ";
		}
		else {
			cout << hex << tmpVal << ",   ";
		}
	}
	cout << "]" << endl;
}

void CryptopalsChallenge3()
{

}

int main()
{
	CryptopalsChallenge1();
	CryptopalsChallenge2();
	return 0;
}