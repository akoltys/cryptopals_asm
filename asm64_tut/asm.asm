.data
secret_num dq 13

.code 
GetValueFromASM proc
	mov rax, secret_num
	ret
GetValueFromASM endp

SubValueFromASM proc
	mov rax, rcx
	sub rax, secret_num
	ret
SubValueFromASM endp
end